# CommandQueue

Creates a menu on the scoreboard that lets you queue up items to have picked up automatically when you open a command cube

![Game screenshot](https://i.imgur.com/ma2kgok.png)

## Features:

-   A full intuitive GUI accessible through a newly added tab on the scoreboard
-   Add items to the end of the queue and remove items from anywhere in the queue
-   Queues for the three item rarities, as well as lunar and boss items, available in separate tabs
    -   Lunar and boss item queues are disabled by default, can be enabled in config
-   Automatically selects items when you interact with a command cube, as long as it's available
    -   If none of the items at the end of the queues are available, the menu opens as normal

## Limitations

There is no controller support - the mod hasn't been tested with a controller and the UI is not in any way made to work with a controller.

## Installation

Copy the `CommandQueue` folder to `Risk of Rain 2/BepInEx/plugins`

## Patch notes: 

- 1.1.0
    - Add expanded menu option (enabled by default)
    - Add support for Lunar and Boss item queues (disabled by default)
    - Change source of item rarities, should help compatibility in some cases
    - Minor improvements to UI (figured out pivots, things can be more dynamic)
- 1.0.0 - Initial release