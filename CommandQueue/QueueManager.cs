﻿using RoR2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CommandQueue
{
    public static class QueueManager
    {
        public struct QueueEntry
        {
            public PickupIndex pickupIndex;
            public int count;
        }

        public enum QueueChange
        {
            Removed, Added, Changed
        }

        public static event Action<Run> OnRunQueueInit;
        public static event Action<QueueChange, ItemTier, int> OnQueueChanged;

        public static Dictionary<ItemTier, List<QueueEntry>> mainQueues = new Dictionary<ItemTier, List<QueueEntry>>
        {
            { ItemTier.Tier1, new List<QueueEntry>() },
            { ItemTier.Tier2, new List<QueueEntry>() },
            { ItemTier.Tier3, new List<QueueEntry>() }
        };

        internal static void Enable()
        {
            Run.onRunStartGlobal += InitQueues;
            UpdateQueueAvailability();
        }

        internal static void Disable()
        {
            Run.onRunStartGlobal -= InitQueues;
        }

        public static void UpdateQueueAvailability()
        {
            var enabledTabs = ModConfig.enabledTabs.Value;
            foreach (var key in mainQueues.Keys.ToArray())
            {
                if (!enabledTabs.Contains(key))
                    mainQueues.Remove(key);
            }
            foreach(ItemTier tier in enabledTabs)
            {
                if (!mainQueues.ContainsKey(tier))
                    mainQueues.Add(tier, new List<QueueEntry>());
            }
        }

        private static void InitQueues(Run run)
        {
            mainQueues.Clear();
            UpdateQueueAvailability();

            OnRunQueueInit?.Invoke(run);
        }

        public static void Enqueue(PickupIndex pickupIndex)
        {
            ItemTier tier = ItemCatalog.GetItemDef(PickupCatalog.GetPickupDef(pickupIndex).itemIndex).tier;

            List<QueueEntry> queue = mainQueues[tier];

            if (queue.Count > 0)
            {
                QueueEntry lastElement = queue[queue.Count - 1];
                if(lastElement.pickupIndex == pickupIndex)
                {
                    lastElement.count++;
                    queue[queue.Count - 1] = lastElement;
                    OnQueueChanged?.Invoke(QueueChange.Changed, tier, queue.Count-1);
                    return;
                }
            }

            QueueEntry newElement = new QueueEntry
            {
                pickupIndex = pickupIndex,
                count = 1
            };

            queue.Add(newElement);
            OnQueueChanged?.Invoke(QueueChange.Added, tier, queue.Count-1);
        }

        public static PickupIndex Peek(ItemTier tier)
        {
            var queue = mainQueues[tier];
            if (queue.Count == 0)
                return PickupIndex.none;
            return queue[0].pickupIndex;
        }

        public static PickupIndex Pop(ItemTier tier)
        {
            return Remove(tier, 0);
        }

        public static PickupIndex Remove(ItemTier tier, int index)
        {
            var queue = mainQueues[tier];
            var entry = queue[index];
            entry.count--;
            if(entry.count <= 0)
            {
                queue.RemoveAt(index);
                OnQueueChanged?.Invoke(QueueChange.Removed, tier, index);
            }
            else
            {
                queue[index] = entry;
                OnQueueChanged?.Invoke(QueueChange.Changed, tier, index);
            }
            return entry.pickupIndex;
        }

        public static PickupIndex[] PeekAll()
        {
            return new PickupIndex[]{
                Peek(ItemTier.Tier1),
                Peek(ItemTier.Tier2),
                Peek(ItemTier.Tier3)
            };
        }

        public static PickupIndex PopByPeekAllIndex(int index)
        {
            ItemTier tier;
            switch(index)
            {
                case 0: tier = ItemTier.Tier1; break;
                case 1: tier = ItemTier.Tier2; break;
                case 2: default: tier = ItemTier.Tier3; break;
            }
            return Pop(tier);
        }
    }
}
