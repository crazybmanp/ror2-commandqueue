﻿using RoR2;
using RoR2.UI;
using System;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CommandQueue
{
    class QueueDisplay : MonoBehaviour
    {
        public GameObject itemIconPrefab = null;
        public float itemIconPrefabWidth = 64f;
        public ItemTier tier;
        private float iconScale = 1;
        private RectTransform rectTransform;
        private List<ItemIcon> icons = new List<ItemIcon>();
        private List<QueueManager.QueueEntry> queue;

        public void Awake()
        {
            rectTransform = transform as RectTransform;
        }

        public void Start()
        {
            iconScale = (rectTransform.rect.height-10) / itemIconPrefabWidth;
        }

        public void LayoutIcon(int index, RectTransform transform)
        {
            transform.anchoredPosition = new Vector2(index * iconScale*(itemIconPrefabWidth+5), 5);
            transform.localScale = new Vector3(iconScale, iconScale, 1);
        }

        public void LayoutIcons(int first = 0)
        {
            for(int i = first; i < icons.Count; i++)
            {
                LayoutIcon(i, icons[i].rectTransform);
            }
        }

        public ItemIcon AllocateIcon(int index)
        {
            ItemIcon icon = Instantiate(itemIconPrefab, rectTransform, false).GetComponent<ItemIcon>();
            RectTransform transform = icon.rectTransform;

            Button button = icon.gameObject.AddComponent<Button>();
            button.onClick.AddListener(delegate
            {
                for(int i = 0; i < icons.Count; i++)
                {
                    if(icons[i] == icon)
                    {
                        QueueManager.Remove(tier, i);
                    }
                }
            });

            transform.anchorMin = new Vector2(0, 1);
            transform.anchorMax = new Vector2(0, 1);
            LayoutIcon(index, transform);
            
            icons.Insert(index, icon);

            return icon;
        }

        public void AllocateIcons(int count, Action<int, ItemIcon> action = null)
        {
            for(int i = icons.Count-1; i >= count; i--)
            {
                Destroy(icons[i].gameObject);
                icons.RemoveAt(i);
            }

            for(int i = icons.Count; i < count; i++)
            {
                ItemIcon icon = AllocateIcon(i);

                action?.Invoke(i, icon);
            }
        }

        public void DestroyUI()
        {
            foreach(var icon in icons)
            {
                Destroy(icon.gameObject);
            }
            icons.Clear();
        }

        public void UpdateUI()
        {
            AllocateIcons(queue.Count);
            UpdateIcons();
        }

        private void UpdateIcon(int i)
        {
            ItemIcon icon = icons[i];
            var entry = queue[i];
            icon.SetItemIndex(PickupCatalog.GetPickupDef(entry.pickupIndex).itemIndex, entry.count);
        }

        private void UpdateIcons()
        {
            for(int i = 0; i < icons.Count; i++)
            {
                UpdateIcon(i);
            }
        }

        private void HandleQueueChange(QueueManager.QueueChange change, ItemTier tier, int index)
        {
            if (!itemIconPrefab)
                return;
            if (tier == this.tier)
            {
                switch(change)
                {
                    case QueueManager.QueueChange.Changed:
                        UpdateIcon(index);
                        break;
                    case QueueManager.QueueChange.Added:
                        AllocateIcon(index);
                        UpdateIcon(index);
                        break;
                    case QueueManager.QueueChange.Removed:
                        Destroy(icons[index].gameObject);
                        icons.RemoveAt(index);
                        LayoutIcons(index);
                        break;
                }
            }
        }

        public void OnEnable()
        {
            if (!itemIconPrefab)
                return;
            queue = QueueManager.mainQueues[tier];
            UpdateUI();
            QueueManager.OnQueueChanged += HandleQueueChange;
        }

        public void OnDisable()
        {
            QueueManager.OnQueueChanged -= HandleQueueChange;
        }
    }
}
